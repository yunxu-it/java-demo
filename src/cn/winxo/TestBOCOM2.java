package cn.winxo;

import cn.winxo.remote.bocom.InfoQueryRQ;
import cn.winxo.remote.bocom.InfoQueryRQB;
import cn.winxo.remote.bocom.RQHead;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.simpleframework.xml.core.Persister;

/**
 * Author: lixiaolong
 * Date: 2017/11/16
 * Email: yunxu.it@outlook.com
 * Desc:
 */
public class TestBOCOM2 {
  public static void main(String[] args) {
    //generatePteReq();
    //generatePteRes();
    generateRequest();
    //generateResponse();
  }

  private static void generateRequest() {
    List<String> bocomAccounts = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      bocomAccounts.add("1245514564156" + i);
    }
    String corpNo = "111";
    String userNo = "222";
    InfoQueryRQ query = new InfoQueryRQ();
    Date date = new Date();
    String dateStr = "201801";
    String timeStr = "1628";
    RQHead head = new RQHead(InfoQueryRQB.TransCode, corpNo, userNo, dateStr,
        timeStr, bocomAccounts.size(), "0");
    query.setHead(head);
    List<InfoQueryRQB> infoQueryRQBS = new ArrayList<>();
    for (String account : bocomAccounts) {
      InfoQueryRQB body = new InfoQueryRQB();
      body.setAccountNo(account);
      infoQueryRQBS.add(body);
    }
    query.setBody(infoQueryRQBS);
    Persister persister = new Persister();
    try {
      persister.write(query, new File("request.xml"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
