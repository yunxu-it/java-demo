package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc 签退交易
 */
@Root(name = "trn-b2e0002-rq")
public class B2E0002RQ {
  /**
   * 客户端时间
   */
  @Path("b2e0002-rq") @Element(name = "custdt") private String customDate;

  public String getCustomDate() {
    return customDate;
  }

  public void setCustomDate(String customDate) {
    this.customDate = customDate;
  }
}
