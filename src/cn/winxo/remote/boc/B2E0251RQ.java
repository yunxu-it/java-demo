package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0251-rq")
public class B2E0251RQ {
  /**
   * 数字签名
   */
  @Element(name = "ceitinfo") private String ceitInfo;
  /**
   * 转账指令ID
   */
  @Path("b2e0251-rq") @Element(name = "insid") private String insID;
  /**
   * 付款人账号
   */
  @Path("b2e0251-rq") @Element(name = "fractn") private FromAccount fromAccount;
  /**
   * 收款人账号
   */
  @Path("b2e0251-rq") @Element(name = "toactn") private ToAccount toAccount;
  /**
   * 付费人账户
   */
  @Path("b2e0251-rq") @Element(name = "feeinfo") private FeeInfo feeInfo;
  /**
   * 金额
   */
  @Path("b2e0251-rq") @Element(name = "trnamt") private String trnamt;
  /**
   * 货币
   */
  @Path("b2e0251-rq") @Element(name = "trncur") private String trncur;
  /**
   * 客户处理优先级
   */
  @Path("b2e0251-rq") @Element(name = "priolv") private String priolv;
  /**
   * 附言
   */
  @Path("b2e0251-rq") @Element(name = "furinfo") private String furinfo;

  @Root
  static class FromAccount {
    @Element(name = "fribkn") private String fribkn;
    @Element(name = "actacn") private String actacn;
    @Element(name = "actnam") private String actname;
  }

  @Root
  static class ToAccount {
    @Element(name = "toibkn") private String toibkn;
    @Element(name = "actacn") private String actacn;
    @Element(name = "toname") private String toname;
    @Element(name = "type") private String type;
  }

  @Root
  static class FeeInfo {
    @Element(name = "ibknum") private String ibknum;
    @Element(name = "actacn") private String actacn;
  }
}
