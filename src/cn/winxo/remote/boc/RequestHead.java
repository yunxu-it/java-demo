package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/12/27
 * @desc
 */
@Root(name = "head")
public class RequestHead {
  @Element(name = "termid")
  private String termId;
  @Element(name = "trnid")
  private String trnid;
  @Element(name = "custid")
  private String custid;
  @Element(name = "cusopr")
  private String cusopr;
  @Element(name = "trncod")
  private String trncod;
  @Element(name = "token", required = false)
  private String token;

  public RequestHead() {
  }

  public RequestHead(String termId, String trnid, String custid, String cusopr,
      String trncod, String token) {
    this.termId = termId;
    this.trnid = trnid;
    this.custid = custid;
    this.cusopr = cusopr;
    this.trncod = trncod;
    this.token = token;
  }

  public String getTermId() {
    return termId;
  }

  public void setTermId(String termId) {
    this.termId = termId;
  }

  public String getTrnid() {
    return trnid;
  }

  public void setTrnid(String trnid) {
    this.trnid = trnid;
  }

  public String getCustid() {
    return custid;
  }

  public void setCustid(String custid) {
    this.custid = custid;
  }

  public String getCusopr() {
    return cusopr;
  }

  public void setCusopr(String cusopr) {
    this.cusopr = cusopr;
  }

  public String getTrncod() {
    return trncod;
  }

  public void setTrncod(String trncod) {
    this.trncod = trncod;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
