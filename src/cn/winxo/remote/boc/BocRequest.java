package cn.winxo.remote.boc;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.ElementListUnion;
import org.simpleframework.xml.ElementUnion;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/12/27
 * @desc
 */
@Root(name = "bocb2e")
public class BocRequest {
  //@Attribute(name = "version")
  //private String version = "100";
  //@Attribute(name = "security")
  //private String security = "true";
  //@Attribute(name = "lang")
  //private String lang = "chs";
  @Element(name = "head")
  private RequestHead head;
  @ElementUnion({
      @Element(name = "trn-b2e0002-rq", type = B2E0002RQ.class, required = false)
  })
  @Path("trans")
  private Object trans;

  @ElementListUnion({
      @ElementList(name = "trn-b2e0002-rq", entry = "b2e0002-rq", required = false, type = B2E0002RQ.class)
  })
  @Path("trans")
  private Object transList;

  public RequestHead getHead() {
    return head;
  }

  public void setHead(RequestHead head) {
    this.head = head;
  }

  public Object getTrans() {
    return trans;
  }

  public void setTrans(Object trans) {
    this.trans = trans;
  }

  public Object getTransList() {
    return transList;
  }

  public void setTransList(Object transList) {
    this.transList = transList;
  }
}
