package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0007-rs")
public class B2E0007RS {
  /**
   * 报文处理状态
   */
  @Element(name = "status") private ResStatus msgStatus;

  /**
   * 交易处理状态
   */
  @Path("b2e0007-rs") @Element(name = "status") private ResStatus status;
  /**
   * 转账指令ID
   */
  @Path("b2e0007-rs") @Element(name = "insid") private String insID;
  /**
   * 网银交易流水号
   */
  @Path("b2e0007-rs") @Element(name = "obssid") private String obssID;
  /**
   * 银行主机流水号
   */
  @Path("b2e0007-rs") @Element(name = "hostseqno") private String hostSeqNo;

  public ResStatus getMsgStatus() {
    return msgStatus;
  }

  public void setMsgStatus(ResStatus msgStatus) {
    this.msgStatus = msgStatus;
  }

  public ResStatus getStatus() {
    return status;
  }

  public void setStatus(ResStatus status) {
    this.status = status;
  }

  public String getInsID() {
    return insID;
  }

  public void setInsID(String insID) {
    this.insID = insID;
  }

  public String getObssID() {
    return obssID;
  }

  public void setObssID(String obssID) {
    this.obssID = obssID;
  }

  public String getHostSeqNo() {
    return hostSeqNo;
  }

  public void setHostSeqNo(String hostSeqNo) {
    this.hostSeqNo = hostSeqNo;
  }
}
