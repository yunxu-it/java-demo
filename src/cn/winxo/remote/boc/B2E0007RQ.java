package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0007-rq")
public class B2E0007RQ {
  /**
   * 转账指令ID
   */
  @Path("b2e0007-rq") @Element(name = "insid") private String insID;
  /**
   * 网银交易流水号
   */
  @Path("b2e0007-rq") @Element(name = "obssid") private String obssID;

  public String getInsID() {
    return insID;
  }

  public void setInsID(String insID) {
    this.insID = insID;
  }

  public String getObssID() {
    return obssID;
  }

  public void setObssID(String obssID) {
    this.obssID = obssID;
  }
}
