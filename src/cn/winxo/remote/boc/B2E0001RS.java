package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0001-rs")
public class B2E0001RS {
  /**
   * 服务器日期
   */
  @Element(name = "serverdt") private String serverDate;
  /**
   * 交易验证标识
   */
  @Element(name = "token") private String token;
  /**
   * 登录响应状态
   */
  @Element(name = "status") private ResStatus status;
}
