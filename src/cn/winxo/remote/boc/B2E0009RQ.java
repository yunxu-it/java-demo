package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0009-rq")
public class B2E0009RQ {
  /**
   * 数字签名
   */
  @Element(name = "ceitinfo") private String ceitInfo;
  /**
   * 交易类型
   */
  @Element(name = "transtype") private String transtype;
  /**
   * 转账指令ID
   */
  @Path("b2e0009-rq") @Element(name = "insid") private String insID;
  /**
   * 网银交易流水号
   */
  @Path("b2e0009-rq") @Element(name = "obssid") private String obssID;
  /**
   * 付款人账号
   */
  @Path("b2e0009-rq") @Element(name = "fractn") private FromAccount fromAccount;
  /**
   * 收款人账户
   */
  @Path("b2e0009-rq") @Element(name = "toactn") private ToAccount toAccount;
  /**
   * 转账金额
   */
  @Path("b2e0009-rq") @Element(name = "trnamt") private String trnamt;
  /**
   * 转账货币
   */
  @Path("b2e0009-rq") @Element(name = "trncur") private String trncur;
  /**
   * 银行处理优先级
   */
  @Path("b2e0009-rq") @Element(name = "priolv") private String priolv;
  /**
   * 用途
   */
  @Path("b2e0009-rq") @Element(name = "furinfo") private String furinfo;
  /**
   * 要求的转账日期
   */
  @Path("b2e0009-rq") @Element(name = "trfdate") private String trfdate;
  /**
   * 要求的转账时间，可空
   */
  @Path("b2e0009-rq") @Element(name = "trftime") private String trftime;
  /**
   * 手续费账号
   */
  @Path("b2e0009-rq") @Element(name = "comacn") private String comacn;

  @Root
  static class FromAccount {
    /**
     * 联行号
     */
    @Element(name = "fribkn") private String fribkn;
    /**
     * 付款账号
     */
    @Element(name = "actacn") private String actacn;
    /**
     * 付款人名称
     */
    @Element(name = "actnam") private String acntname;

    public String getFribkn() {
      return fribkn;
    }

    public void setFribkn(String fribkn) {
      this.fribkn = fribkn;
    }

    public String getActacn() {
      return actacn;
    }

    public void setActacn(String actacn) {
      this.actacn = actacn;
    }

    public String getAcntname() {
      return acntname;
    }

    public void setAcntname(String acntname) {
      this.acntname = acntname;
    }
  }

  @Root
  static class ToAccount {
    /**
     * 收款行联行号
     */
    @Element(name = "toibkn") private String toibkn;
    /**
     * 收款账号
     */
    @Element(name = "actacn") private String actacn;
    /**
     * 收款人名称
     */
    @Element(name = "toname") private String toname;
    /**
     * 收款人地址
     */
    @Element(name = "toaddr") private String toaddr;
    /**
     * 收款人开户行名称
     */
    @Element(name = "tobknm") private String tobknm;

    public String getToibkn() {
      return toibkn;
    }

    public void setToibkn(String toibkn) {
      this.toibkn = toibkn;
    }

    public String getActacn() {
      return actacn;
    }

    public void setActacn(String actacn) {
      this.actacn = actacn;
    }

    public String getToname() {
      return toname;
    }

    public void setToname(String toname) {
      this.toname = toname;
    }

    public String getToaddr() {
      return toaddr;
    }

    public void setToaddr(String toaddr) {
      this.toaddr = toaddr;
    }

    public String getTobknm() {
      return tobknm;
    }

    public void setTobknm(String tobknm) {
      this.tobknm = tobknm;
    }
  }

  public String getCeitInfo() {
    return ceitInfo;
  }

  public void setCeitInfo(String ceitInfo) {
    this.ceitInfo = ceitInfo;
  }

  public String getTranstype() {
    return transtype;
  }

  public void setTranstype(String transtype) {
    this.transtype = transtype;
  }

  public String getInsID() {
    return insID;
  }

  public void setInsID(String insID) {
    this.insID = insID;
  }

  public String getObssID() {
    return obssID;
  }

  public void setObssID(String obssID) {
    this.obssID = obssID;
  }

  public FromAccount getFromAccount() {
    return fromAccount;
  }

  public void setFromAccount(FromAccount fromAccount) {
    this.fromAccount = fromAccount;
  }

  public ToAccount getToAccount() {
    return toAccount;
  }

  public void setToAccount(ToAccount toAccount) {
    this.toAccount = toAccount;
  }

  public String getTrnamt() {
    return trnamt;
  }

  public void setTrnamt(String trnamt) {
    this.trnamt = trnamt;
  }

  public String getTrncur() {
    return trncur;
  }

  public void setTrncur(String trncur) {
    this.trncur = trncur;
  }

  public String getPriolv() {
    return priolv;
  }

  public void setPriolv(String priolv) {
    this.priolv = priolv;
  }

  public String getFurinfo() {
    return furinfo;
  }

  public void setFurinfo(String furinfo) {
    this.furinfo = furinfo;
  }

  public String getTrfdate() {
    return trfdate;
  }

  public void setTrfdate(String trfdate) {
    this.trfdate = trfdate;
  }

  public String getTrftime() {
    return trftime;
  }

  public void setTrftime(String trftime) {
    this.trftime = trftime;
  }

  public String getComacn() {
    return comacn;
  }

  public void setComacn(String comacn) {
    this.comacn = comacn;
  }
}
