package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc 今日及历史交易信息查询
 */
@Root(name = "trn-b2e0035-rs")
public class B2E0035RS {
  /**
   * 报文处理状态
   */
  @Element(name = "status") private ResStatus msgStatus;
  /**
   * 本次返回笔数
   */
  @Element(name = "totalnum") private String totalNum;
  /**
   * 总记录数
   */
  @Element(name = "notenum") private String noteNum;
  /**
   * 交易处理状态
   */
  @Path("b2e0035-rs") @Element(name = "status") private ResStatus status;
  /**
   * 付款方信息
   */
  @Path("b2e0035-rs") @Element(name = "fractn") private FromAccount fromAccount;
  /**
   * 收款方信息
   */
  @Path("b2e0035-rs") @Element(name = "toactn") private ToAccount toAccount;
  /**
   * 被代理行号
   */
  @Path("b2e0035-rs") @Element(name = "mactibkn") private String mactibkn;
  /**
   * 被代理账号
   */
  @Path("b2e0035-rs") @Element(name = "mactacn") private String mactacn;
  /**
   * 被代理账户名
   */
  @Path("b2e0035-rs") @Element(name = "mactname") private String mactname;
  /**
   * 被代理账户开户行名
   */
  @Path("b2e0035-rs") @Element(name = "mactbank") private String mactbank;
  /**
   * 旧线是10位凭证号或传票号
   * 新线是9位流水号
   */
  @Path("b2e0035-rs") @Element(name = "vchnum") private String vchnum;
  /**
   * 记录标识号
   */
  @Path("b2e0035-rs") @Element(name = "transid") private String transId;
  /**
   * 客户业务编号
   */
  @Path("b2e0035-rs") @Element(name = "insid") private String insid;
  /**
   * 交易日期
   */
  @Path("b2e0035-rs") @Element(name = "txndate") private String txndate;
  /**
   * 交易时间
   */
  @Path("b2e0035-rs") @Element(name = "txntime") private String txntime;
  /**
   * 金额
   */
  @Path("b2e0035-rs") @Element(name = "txnamt") private String txnamt;
  /**
   * 交易后金额
   */
  @Path("b2e0035-rs") @Element(name = "acctbal") private String acctbal;
  /**
   * 可用余额
   */
  @Path("b2e0035-rs") @Element(name = "avlbal") private String avlbal;
  /**
   * 冻结金额
   */
  @Path("b2e0035-rs") @Element(name = "frzamt") private String frzamt;
  /**
   * 透支额度
   */
  @Path("b2e0035-rs") @Element(name = "overdramt") private String overdramt;
  /**
   * 可用透支额度
   */
  @Path("b2e0035-rs") @Element(name = "avloverdramt") private String avloverdramt;
  /**
   * 用途
   */
  @Path("b2e0035-rs") @Element(name = "userinfo") private String userinfo;
  /**
   * 附言
   */
  @Path("b2e0035-rs") @Element(name = "furinfo") private String furinfo;
  /**
   * 业务类型
   */
  @Path("b2e0035-rs") @Element(name = "transtype") private String transtype;
  /**
   * 新业务类型
   */
  @Path("b2e0035-rs") @Element(name = "bustype") private String bustype;
  /**
   * 货币名称
   */
  @Path("b2e0035-rs") @Element(name = "trncur") private String trncur;
  /**
   * 来往账标识
   */
  @Path("b2e0035-rs") @Element(name = "direction") private String direction;
  /**
   * 费用账户
   */
  @Path("b2e0035-rs") @Element(name = "feeact") private String feeact;
  /**
   * 费用金额
   */
  @Path("b2e0035-rs") @Element(name = "feeamt") private String feeamt;
  /**
   * 费用货币
   */
  @Path("b2e0035-rs") @Element(name = "feecur") private String feecur;
  /**
   * 起息日期
   */
  @Path("b2e0035-rs") @Element(name = "valdat") private String valdat;
  /**
   * 凭证类型
   */
  @Path("b2e0035-rs") @Element(name = "vouchtp") private String vouchtp;
  /**
   * 凭证号码
   */
  @Path("b2e0035-rs") @Element(name = "vouvhnum") private String vouvhnum;
  /**
   * 汇率
   */
  @Path("b2e0035-rs") @Element(name = "fxrate") private String fxrate;
  /**
   * 整合信息
   */
  @Path("b2e0035-rs") @Element(name = "interinfo") private String interinfo;
  /**
   * 预留项1
   */
  @Path("b2e0035-rs") @Element(name = "reserve1") private String reserve1;
  /**
   * 预留项2
   */
  @Path("b2e0035-rs") @Element(name = "reserve2") private String reserve2;
  /**
   * 预留项3
   */
  @Path("b2e0035-rs") @Element(name = "reserve3") private String reserve3;

  /**
   * 付款方信息
   */
  @Root
  static class FromAccount {
    @Element(name = "ibknum") private String ibknum;
    @Element(name = "actacn") private String actacn;
    @Element(name = "acntname") private String acntname;
    @Element(name = "ibkname") private String ibkname;

    public String getIbknum() {
      return ibknum;
    }

    public void setIbknum(String ibknum) {
      this.ibknum = ibknum;
    }

    public String getActacn() {
      return actacn;
    }

    public void setActacn(String actacn) {
      this.actacn = actacn;
    }

    public String getAcntname() {
      return acntname;
    }

    public void setAcntname(String acntname) {
      this.acntname = acntname;
    }

    public String getIbkname() {
      return ibkname;
    }

    public void setIbkname(String ibkname) {
      this.ibkname = ibkname;
    }
  }

  @Root
  static class ToAccount {
    @Element(name = "toibkn") private String toibkn;
    @Element(name = "actacn") private String actacn;
    @Element(name = "toname") private String toname;
    @Element(name = "tobank") private String tobank;
  }
}
