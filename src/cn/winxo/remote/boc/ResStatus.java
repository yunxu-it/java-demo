package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/30
 * @desc 应答报文状态值
 */
@Root
public class ResStatus {

  /**
   * 响应码
   */
  @Element(name = "rspcod") private String rspCode;
  /**
   * 响应消息
   */
  @Element(name = "rspmsg") private String rspMsg;

  public String getRspCode() {
    return rspCode;
  }

  public void setRspCode(String rspCode) {
    this.rspCode = rspCode;
  }

  public String getRspMsg() {
    return rspMsg;
  }

  public void setRspMsg(String rspMsg) {
    this.rspMsg = rspMsg;
  }
}