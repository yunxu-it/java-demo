package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0061-rq")
public class B2E0061RQ {
  /**
   * 数字签名
   */
  @Element(name = "ceitinfo") private String ceitInfo;
  /**
   * 交易类型
   */
  @Element(name = "transtype") private String transtype;
  /**
   * 转账指令ID
   */
  @Path("b2e0061-rq") @Element(name = "insid") private String insID;
  /**
   * 网银交易流水号
   */
  @Path("b2e0061-rq") @Element(name = "obssid") private String obssID;
  /**
   * 付款人账号
   */
  @Path("b2e0061-rq") @Element(name = "fractn") private FromAccount fromAccount;
  /**
   * 收款人账号
   */
  @Path("b2e0061-rq") @Element(name = "toactn") private ToAccount toAccount;
  /**
   * 转账金额
   */
  @Path("b2e0061-rq") @Element(name = "trnamt") private String trnamt;
  /**
   * 转账货币
   */
  @Path("b2e0061-rq") @Element(name = "trncur") private String trncur;
  /**
   * 银行处理优先级
   */
  @Path("b2e0061-rq") @Element(name = "priolv") private String priolv;
  /**
   * 客户处理优先级
   */
  @Path("b2e0061-rq") @Element(name = "cuspriolv") private String cuspriolv;
  /**
   * 用途
   */
  @Path("b2e0061-rq") @Element(name = "furinfo") private String furinfo;
  /**
   * 要求的转账日期
   */
  @Path("b2e0061-rq") @Element(name = "trfdate") private String trfdate;
  /**
   * 要求的转账时间
   */
  @Path("b2e0061-rq") @Element(name = "trftime") private String trftime;
  /**
   * 支付费用账号
   */
  @Path("b2e0061-rq") @Element(name = "comacn") private String comacn;
  /**
   * 收款人是否中行账号
   */
  @Path("b2e0061-rq") @Element(name = "bocflag") private String bocflag;

  @Root
  static class FromAccount {
    @Element(name = "fribkn") private String fribkn;
    @Element(name = "actacn") private String actacn;
    @Element(name = "actnam") private String actnam;
  }

  @Root
  static class ToAccount {
    @Element(name = "toibkn") private String toibkn;
    @Element(name = "acttyp") private String acttyp;
    @Element(name = "actacn") private String actacn;
    @Element(name = "toname") private String toname;
    @Element(name = "tobknm") private String tobknm;
    @Element(name = "toaddr") private String toaddr;
  }
}
