package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0005-rs")
public class B2E0005RS {
  /**
   * 报文处理状态
   */
  @Element(name = "status") private ResStatus msgStatus;

  /**
   * 交易处理状态
   */
  @Path("b2e0005-rs") @Element(name = "status") private ResStatus status;
  /**
   * 账号
   */
  @Path("b2e0005-rs") @Element(name = "account") private Account account;
  /**
   * 当前余额
   */
  @Path("b2e0005-rs") @Element(name = "balance") private Balance balance;
  /**
   * 日期
   */
  @Path("b2e0005-rs") @Element(name = "baldat") private String baldat;


  public ResStatus getMsgStatus() {
    return msgStatus;
  }

  public void setMsgStatus(ResStatus msgStatus) {
    this.msgStatus = msgStatus;
  }

  public ResStatus getStatus() {
    return status;
  }

  public void setStatus(ResStatus status) {
    this.status = status;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public Balance getBalance() {
    return balance;
  }

  public void setBalance(Balance balance) {
    this.balance = balance;
  }

  public String getBaldat() {
    return baldat;
  }

  public void setBaldat(String baldat) {
    this.baldat = baldat;
  }

  @Root
  static class Account {

    /**
     * 联行号
     */
    @Element(name = "ibknum") private String bankNum;
    /**
     * 账号
     */
    @Element(name = "actacn") private String account;
    /**
     * 货币码
     */
    @Element(name = "curcde") private String currencyCode;
    /**
     * 户名
     */
    @Element(name = "actname") private String accountName;

    public String getBankNum() {
      return bankNum;
    }

    public void setBankNum(String bankNum) {
      this.bankNum = bankNum;
    }

    public String getAccount() {
      return account;
    }

    public void setAccount(String account) {
      this.account = account;
    }

    public String getCurrencyCode() {
      return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
      this.currencyCode = currencyCode;
    }

    public String getAccountName() {
      return accountName;
    }

    public void setAccountName(String accountName) {
      this.accountName = accountName;
    }
  }

  @Root
  static class Balance {

    /**
     * 账面余额
     */
    @Element(name = "bokbal") private String bokbal;
    /**
     * 有效余额
     */
    @Element(name = "avabal") private String avabal;
    /**
     * 圈存金额
     */
    @Element(name = "stpamt") private String stpamt;
    /**
     * 透支限额
     */
    @Element(name = "ovramt") private String ovramt;

    public String getBokbal() {
      return bokbal;
    }

    public void setBokbal(String bokbal) {
      this.bokbal = bokbal;
    }

    public String getAvabal() {
      return avabal;
    }

    public void setAvabal(String avabal) {
      this.avabal = avabal;
    }

    public String getStpamt() {
      return stpamt;
    }

    public void setStpamt(String stpamt) {
      this.stpamt = stpamt;
    }

    public String getOvramt() {
      return ovramt;
    }

    public void setOvramt(String ovramt) {
      this.ovramt = ovramt;
    }
  }
}
