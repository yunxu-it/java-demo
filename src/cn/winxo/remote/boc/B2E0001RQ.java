package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc 登录请求信息
 */
@Root(name = "trn-b2e0001-rq")
public class B2E0001RQ {
  /**
   * 用户签名信息，该标签由前置机自动添加，企业无须上送
   */
  @Element(name = "ceitinfo") private String ceitInfo;
  /**
   * 客户端时间
   */
  @Path("b2e0001-rq") @Element(name = "custdt") private String customDate;
  /**
   * 登录密码
   */
  @Path("b2e0001-rq") @Element(name = "oprpwd") private String loginPassword;

  public String getCeitInfo() {
    return ceitInfo;
  }

  public void setCeitInfo(String ceitInfo) {
    this.ceitInfo = ceitInfo;
  }

  public String getCustomDate() {
    return customDate;
  }

  public void setCustomDate(String customDate) {
    this.customDate = customDate;
  }

  public String getLoginPassword() {
    return loginPassword;
  }

  public void setLoginPassword(String loginPassword) {
    this.loginPassword = loginPassword;
  }
}
