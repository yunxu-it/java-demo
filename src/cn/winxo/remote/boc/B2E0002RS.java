package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0002-rs")
public class B2E0002RS {
  /**
   * 服务器日期
   */
  @Element(name = "serverdt") private String serverDate;
  /**
   * 响应状态值
   */
  @Element(name = "status") private ResStatus status;
}
