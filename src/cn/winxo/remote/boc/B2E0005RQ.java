package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0005-rq")
public class B2E0005RQ {
  /**
   * 账号
   */
  @Path("b2e0005-rq") @Element(name = "account") private Account account;

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  @Root
  static class Account {

    /**
     * 联行号
     */
    @Element(name = "ibknum") private String bankNum;
    /**
     * 账号
     */
    @Element(name = "actacn") private String account;

    public String getBankNum() {
      return bankNum;
    }

    public void setBankNum(String bankNum) {
      this.bankNum = bankNum;
    }

    public String getAccount() {
      return account;
    }

    public void setAccount(String account) {
      this.account = account;
    }
  }
}
