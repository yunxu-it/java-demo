package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0035-rq")
public class B2E0035RQ {
  /**
   * 查询账户的联行号
   */
  @Path("b2e0035-rq") @Element(name = "ibknum") private String ibknum;
  /**
   * 查询账户的账号
   */
  @Path("b2e0035-rq") @Element(name = "actacn") private String actacn;
  /**
   * 查询类型
   */
  @Path("b2e0035-rq") @Element(name = "type") private String type;
  /**
   * 日期范围
   */
  @Path("b2e0035-rq") @Element(name = "datescope") private Scope dateScope;
  /**
   * 查询金额范围
   */
  @Path("b2e0035-rq") @Element(name = "amountscope") private Scope amountScope;
  /**
   * 本次查询的交易起始位置
   */
  @Path("b2e0035-rq") @Element(name = "begnum") private String begNum;
  /**
   * 查询记录数
   */
  @Path("b2e0035-rq") @Element(name = "recnum") private String recNum;
  /**
   * 来往账标识
   */
  @Path("b2e0035-rq") @Element(name = "direction") private String direction;

  public String getIbknum() {
    return ibknum;
  }

  public void setIbknum(String ibknum) {
    this.ibknum = ibknum;
  }

  public String getActacn() {
    return actacn;
  }

  public void setActacn(String actacn) {
    this.actacn = actacn;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getBegNum() {
    return begNum;
  }

  public void setBegNum(String begNum) {
    this.begNum = begNum;
  }

  public String getRecNum() {
    return recNum;
  }

  public void setRecNum(String recNum) {
    this.recNum = recNum;
  }

  public String getDirection() {
    return direction;
  }

  public void setDirection(String direction) {
    this.direction = direction;
  }

  public Scope getDateScope() {
    return dateScope;
  }

  public void setDateScope(Scope dateScope) {
    this.dateScope = dateScope;
  }

  public Scope getAmountScope() {
    return amountScope;
  }

  public void setAmountScope(Scope amountScope) {
    this.amountScope = amountScope;
  }

  @Root
  static class Scope {
    /**
     * 开始日期
     */
    @Element(name = "from") private String from;
    /**
     * 截止日期
     */
    @Element(name = "to") private String to;

    public String getFrom() {
      return from;
    }

    public void setFrom(String from) {
      this.from = from;
    }

    public String getTo() {
      return to;
    }

    public void setTo(String to) {
      this.to = to;
    }
  }
}
