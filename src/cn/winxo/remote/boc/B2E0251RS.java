package cn.winxo.remote.boc;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/29
 * @desc
 */
@Root(name = "trn-b2e0251-rs")
public class B2E0251RS {
  /**
   * 报文处理状态
   */
  @Element(name = "status") private ResStatus msgStatus;

  @Path("b2e0251-rs") @Element(name = "status") private ResStatus status;
  @Path("b2e0251-rs") @Element(name = "insid") private String insID;
  @Path("b2e0251-rs") @Element(name = "obssid") private String obssID;

  public ResStatus getMsgStatus() {
    return msgStatus;
  }

  public void setMsgStatus(ResStatus msgStatus) {
    this.msgStatus = msgStatus;
  }

  public ResStatus getStatus() {
    return status;
  }

  public void setStatus(ResStatus status) {
    this.status = status;
  }

  public String getInsID() {
    return insID;
  }

  public void setInsID(String insID) {
    this.insID = insID;
  }

  public String getObssID() {
    return obssID;
  }

  public void setObssID(String obssID) {
    this.obssID = obssID;
  }

}
