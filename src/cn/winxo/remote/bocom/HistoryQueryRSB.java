package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 310301/310201 查询历史明细
 */
@Root
public class HistoryQueryRSB {

  /**
   * 带中文域名的多域串
   */
  @Element(name = "serial_record") private String serialRecord;
  /**
   * 字段数
   */
  @Element(name = "field_num") private int fieldNum;
  /**
   * 记录数
   */
  @Element(name = "record_num") private int recordNum;
  /**
   * 文件标志
   */
  @Element(name = "file_flag") private String fileFlag;
  /**
   * 文件名
   */
  @Element(name = "filename") private String fileName;

  public HistoryQueryRSB(String serialRecord, int fieldNum, int recordNum,
      String fileFlag, String fileName) {
    this.serialRecord = serialRecord;
    this.fieldNum = fieldNum;
    this.recordNum = recordNum;
    this.fileFlag = fileFlag;
    this.fileName = fileName;
  }

  public String getSerialRecord() {
    return serialRecord;
  }

  public void setSerialRecord(String serialRecord) {
    this.serialRecord = serialRecord;
  }

  public int getFieldNum() {
    return fieldNum;
  }

  public void setFieldNum(int fieldNum) {
    this.fieldNum = fieldNum;
  }

  public int getRecordNum() {
    return recordNum;
  }

  public void setRecordNum(int recordNum) {
    this.recordNum = recordNum;
  }

  public String getFileFlag() {
    return fileFlag;
  }

  public void setFileFlag(String fileFlag) {
    this.fileFlag = fileFlag;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
}
