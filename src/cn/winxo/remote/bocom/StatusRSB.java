package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 000001 交易签到
 */
@Root
public class StatusRSB {

  /**
   * 流水号
   */
  @Element(name = "ogl_serial_no")
  private String serialNo;
  /**
   * 企业用户号
   */
  @Element(name = "stat")
  private String status;
  /**
   * 企业用户号
   */
  @Element(name = "err_msg")
  private String errMsg;
  /**
   * 企业用户号
   */
  @Element(name = "pay_acno")
  private String pay_acno;
  /**
   * 企业用户号
   */
  @Element(name = "pay_bank_no")
  private String pay_bank_no;
  /**
   * 企业用户号
   */
  @Element(name = "rcv_acno")
  private String rcv_acno;
  /**
   * 企业用户号
   */
  @Element(name = "rcv_bank_no")
  private String rcv_bank_no;
  @Element(name = "amt")
  private String amt;
  @Element(name = "cert_no")
  private String cert_no;

  public String getSerialNo() {
    return serialNo;
  }

  public void setSerialNo(String serialNo) {
    this.serialNo = serialNo;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getErrMsg() {
    return errMsg;
  }

  public void setErrMsg(String errMsg) {
    this.errMsg = errMsg;
  }

  public String getPay_acno() {
    return pay_acno;
  }

  public void setPay_acno(String pay_acno) {
    this.pay_acno = pay_acno;
  }

  public String getPay_bank_no() {
    return pay_bank_no;
  }

  public void setPay_bank_no(String pay_bank_no) {
    this.pay_bank_no = pay_bank_no;
  }

  public String getRcv_acno() {
    return rcv_acno;
  }

  public void setRcv_acno(String rcv_acno) {
    this.rcv_acno = rcv_acno;
  }

  public String getRcv_bank_no() {
    return rcv_bank_no;
  }

  public void setRcv_bank_no(String rcv_bank_no) {
    this.rcv_bank_no = rcv_bank_no;
  }

  public String getAmt() {
    return amt;
  }

  public void setAmt(String amt) {
    this.amt = amt;
  }

  public String getCert_no() {
    return cert_no;
  }

  public void setCert_no(String cert_no) {
    this.cert_no = cert_no;
  }
}
