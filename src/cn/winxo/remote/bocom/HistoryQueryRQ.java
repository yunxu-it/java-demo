package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 明细查询
 */
@Root(name = "ap")
public class HistoryQueryRQ {

  @Element(name = "head")
  private RQHead head;
  @Element(name = "body")
  private HistoryQueryRQB body;

  public RQHead getHead() {
    return head;
  }

  public void setHead(RQHead head) {
    this.head = head;
  }

  public HistoryQueryRQB getBody() {
    return body;
  }

  public void setBody(HistoryQueryRQB body) {
    this.body = body;
  }
}
