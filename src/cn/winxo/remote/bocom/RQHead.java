package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 请求报文
 */
@Root
public class RQHead {

  /**
   * 交易码
   */
  @Element(name = "tr_code")
  private String transCode;
  /**
   * 企业代码，银行提供
   */
  @Element(name = "corp_no")
  private String corpNo;
  /**
   * 企业用户号，银行提供
   */
  @Element(name = "user_no")
  private String userNo;
  /**
   * 发起方序号，企业方产生
   */
  @Element(name = "req_no", required = false)
  private String requestNo;
  /**
   * 交易日期
   */
  @Element(name = "tr_acdt")
  private String transDate;
  /**
   * 交易时间
   */
  @Element(name = "tr_time")
  private String transTime;
  /**
   * 院子交易数,缺省为1
   */
  @Element(name = "atom_tr_count")
  private int atomTransCount;
  /**
   * 渠道标志
   */
  @Element(name = "channel")
  private String channel;
  /**
   * 保留字段
   */
  @Element(name = "reserved", required = false)
  private String reserved;

  public RQHead() {
  }

  public RQHead(String transCode, String corpNo, String userNo,
      String transDate, String transTime, int atomTransCount, String channel) {
    this.transCode = transCode;
    this.corpNo = corpNo;
    this.userNo = userNo;
    this.transDate = transDate;
    this.transTime = transTime;
    this.atomTransCount = atomTransCount;
    this.channel = channel;
  }


  public String getTransCode() {
    return transCode;
  }

  public void setTransCode(String transCode) {
    this.transCode = transCode;
  }

  public String getCorpNo() {
    return corpNo;
  }

  public void setCorpNo(String corpNo) {
    this.corpNo = corpNo;
  }

  public String getUserNo() {
    return userNo;
  }

  public void setUserNo(String userNo) {
    this.userNo = userNo;
  }

  public String getRequestNo() {
    return requestNo;
  }

  public void setRequestNo(String requestNo) {
    this.requestNo = requestNo;
  }

  public String getTransDate() {
    return transDate;
  }

  public void setTransDate(String transDate) {
    this.transDate = transDate;
  }

  public String getTransTime() {
    return transTime;
  }

  public void setTransTime(String transTime) {
    this.transTime = transTime;
  }

  public int getAtomTransCount() {
    return atomTransCount;
  }

  public void setAtomTransCount(int atomTransCount) {
    this.atomTransCount = atomTransCount;
  }

  public String getChannel() {
    return channel;
  }

  public void setChannel(String channel) {
    this.channel = channel;
  }

  public String getReserved() {
    return reserved;
  }

  public void setReserved(String reserved) {
    this.reserved = reserved;
  }
}
