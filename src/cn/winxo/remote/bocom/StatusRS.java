package cn.winxo.remote.bocom;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 签到应答报文
 */
@Root(name = "ap")
public class StatusRS {

  @Element(name = "head")
  private ResponseHead head;
  @ElementList(entry = "body", inline = true)
  private List<StatusRSB> body;

  public ResponseHead getHead() {
    return head;
  }

  public void setHead(ResponseHead head) {
    this.head = head;
  }

  public List<StatusRSB> getBody() {
    return body;
  }

  public void setBody(List<StatusRSB> body) {
    this.body = body;
  }
}
