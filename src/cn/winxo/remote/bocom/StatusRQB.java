package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc <pre>
 *   310204 转账交易结果查询
 * </pre>
 */
@Root
public class StatusRQB {

  public static final String TransCode = "310204";

  /**
   * 查询标志
   */
  @Element(name = "query_flag")
  private String queryFlag;

  /**
   * 流水号
   */
  @Element(name = "ogl_serial_no")
  private String serialNo;

  public String getQueryFlag() {
    return queryFlag;
  }

  public void setQueryFlag(String queryFlag) {
    this.queryFlag = queryFlag;
  }

  public String getSerialNo() {
    return serialNo;
  }

  public void setSerialNo(String serialNo) {
    this.serialNo = serialNo;
  }
}
