package cn.winxo.remote.bocom;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc
 */
@Root(name = "ap")
public class InfoQueryRQ {

  @Element(name = "head")
  private RQHead head;
  @ElementList(inline = true, entry = "body")
  private List<InfoQueryRQB> body;

  public RQHead getHead() {
    return head;
  }

  public void setHead(RQHead head) {
    this.head = head;
  }

  public List<InfoQueryRQB> getBody() {
    return body;
  }

  public void setBody(List<InfoQueryRQB> body) {
    this.body = body;
  }
}
