package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 310101 查询账户信息Body
 */
@Root(name = "body")
public class InfoQueryRQB {

  public static final String TransCode = "310101";

  @Element(name = "acno")
  private String accountNo;

  public String getAccountNo() {
    return accountNo;
  }

  public void setAccountNo(String accountNo) {
    this.accountNo = accountNo;
  }
}
