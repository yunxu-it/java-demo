package cn.winxo.remote.bocom;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 330002 （单/批)私本应答body
 */
@Root
public class PaySelfPersonalRSB {

  /**
   * 网银批次号
   */
  @Element(name = "ebank_batflw", required = false)
  private String ebankBatflw;
  /**
   * 成功笔数
   */
  @Element(name = "ebank_suc_sum", required = false)
  private String payAccount;
  /**
   * 失败笔数
   */
  @Element(name = "ebank_fild_sum", required = false)
  private String type;
  /**
   * 可以笔数
   */
  @Element(name = "ebank_unkown_sum", required = false)
  private String sum;
  /**
   * 扣款明细信息
   */
  @ElementList(name = "tran", entry = "rcd", required = false)
  private List<TransRecordRS> transDetail;


  public String getEbankBatflw() {
    return ebankBatflw;
  }

  public void setEbankBatflw(String ebankBatflw) {
    this.ebankBatflw = ebankBatflw;
  }

  public String getPayAccount() {
    return payAccount;
  }

  public void setPayAccount(String payAccount) {
    this.payAccount = payAccount;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getSum() {
    return sum;
  }

  public void setSum(String sum) {
    this.sum = sum;
  }

  public List<TransRecordRS> getTransDetail() {
    return transDetail;
  }

  public void setTransDetail(List<TransRecordRS> transDetail) {
    this.transDetail = transDetail;
  }
}
