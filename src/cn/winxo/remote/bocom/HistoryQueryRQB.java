package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 310301/310201 查询历史交易明细/查询当日交易明细
 */
@Root
public class HistoryQueryRQB {

  public static final String TransCode = "310301";
  public static final String TransCodeToday = "310201";

  /**
   * 账号
   */
  @Element(name = "acno")
  private String accountNo;
  /**
   * 开始时间（用于历史查询）
   */
  @Element(name = "start_date", required = false)
  private String startDate;
  /**
   * 结束时间（用于历史查询）
   */
  @Element(name = "end_date", required = false)
  private String endDate;

  public String getAccountNo() {
    return accountNo;
  }

  public void setAccountNo(String accountNo) {
    this.accountNo = accountNo;
  }

  public String getStartDate() {
    return startDate;
  }

  public void setStartDate(String startDate) {
    this.startDate = startDate;
  }

  public String getEndDate() {
    return endDate;
  }

  public void setEndDate(String endDate) {
    this.endDate = endDate;
  }
}
