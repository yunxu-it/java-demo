package cn.winxo.remote.bocom;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 330002 （单/批)私本请求
 */
@Root
public class PaySelfPersonalRQB {

  public static final String TransCode = "330002";

  /**
   * 企业凭证编号
   */
  @Element(name = "cert_no")
  private String certNo;
  /**
   * 付款人账号
   */
  @Element(name = "pay_acno")
  private String payAccount;
  /**
   * 签约类型
   */
  @Element(name = "type")
  private String type;
  /**
   * 总笔数
   */
  @Element(name = "sum")
  private int sum;
  /**
   * 总金额
   */
  @Element(name = "sum_amt")
  private String sumAmount;
  /**
   * 月份
   */
  @Element(name = "pay_month")
  private String payMonth;
  /**
   * 附言
   */
  @Element(name = "summary", required = false)
  private String summary;
  /**
   * 协议编号
   */
  @Element(name = "busi_no")
  private String busiNo;
  /**
   * 传票汇总标志
   */
  @Element(name = "mailflag", required = false)
  private String mailFlag;

  /**
   * 扣款明细信息
   */
  @ElementList(name = "tran")
  private List<TransRecordRQ> transDetail;

  public PaySelfPersonalRQB(String certNo, String payAccount, String type, int sum,
      String sumAmount,
      String payMonth, String busiNo,
      List<TransRecordRQ> transDetail) {
    this.certNo = certNo;
    this.payAccount = payAccount;
    this.type = type;
    this.sum = sum;
    this.sumAmount = sumAmount;
    this.payMonth = payMonth;
    this.busiNo = busiNo;
    this.transDetail = transDetail;
  }

  public String getCertNo() {
    return certNo;
  }

  public void setCertNo(String certNo) {
    this.certNo = certNo;
  }

  public String getPayAccount() {
    return payAccount;
  }

  public void setPayAccount(String payAccount) {
    this.payAccount = payAccount;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public int getSum() {
    return sum;
  }

  public void setSum(int sum) {
    this.sum = sum;
  }

  public String getSumAmount() {
    return sumAmount;
  }

  public void setSumAmount(String sumAmount) {
    this.sumAmount = sumAmount;
  }

  public String getPayMonth() {
    return payMonth;
  }

  public void setPayMonth(String payMonth) {
    this.payMonth = payMonth;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getBusiNo() {
    return busiNo;
  }

  public void setBusiNo(String busiNo) {
    this.busiNo = busiNo;
  }

  public String getMailFlag() {
    return mailFlag;
  }

  public void setMailFlag(String mailFlag) {
    this.mailFlag = mailFlag;
  }

  public List<TransRecordRQ> getTransDetail() {
    return transDetail;
  }

  public void setTransDetail(List<TransRecordRQ> transDetail) {
    this.transDetail = transDetail;
  }
}
