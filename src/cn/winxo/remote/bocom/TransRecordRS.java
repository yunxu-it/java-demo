package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 210201 （单/批)私本
 */
@Root
public class TransRecordRS {

  /**
   * 卡号
   */
  @Element(name = "card_no")
  private String cardNo;

  /**
   * 成功标志
   */
  @Element(name = "sucFlg")
  private String successFlag;
  /**
   * 网银流水号
   */
  @Element(name = "flw")
  private String flowNo;

  public TransRecordRS(String cardNo, String successFlag, String flowNo) {
    this.cardNo = cardNo;
    this.successFlag = successFlag;
    this.flowNo = flowNo;
  }

  public String getCardNo() {
    return cardNo;
  }

  public void setCardNo(String cardNo) {
    this.cardNo = cardNo;
  }

  public String getSuccessFlag() {
    return successFlag;
  }

  public void setSuccessFlag(String successFlag) {
    this.successFlag = successFlag;
  }

  public String getFlowNo() {
    return flowNo;
  }

  public void setFlowNo(String flowNo) {
    this.flowNo = flowNo;
  }
}
