package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc
 */
@Root(name = "ap")
public class PayPublicRQ {
  @Element(name = "head") private RQHead head;
  @Element(name = "body") private PayPublicRQB body;

  public RQHead getHead() {
    return head;
  }

  public void setHead(RQHead head) {
    this.head = head;
  }

  public PayPublicRQB getBody() {
    return body;
  }

  public void setBody(PayPublicRQB body) {
    this.body = body;
  }
}
