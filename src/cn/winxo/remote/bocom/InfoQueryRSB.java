package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 310101 查询账户信息应答body
 */
@Root
public class InfoQueryRSB {

  /**
   * 带中文域名的多域串
   */
  @Element(name = "serial_record")
  private String serialRecord;
  /**
   * 字段数
   */
  @Element(name = "field_num")
  private int fieldNum;
  /**
   * 记录数
   */
  @Element(name = "record_num")
  private int recordNum;
  /**
   * 文件标志
   */
  @Element(name = "file_flag")
  private String fileFlag;
  /**
   * 文件名
   */
  @Element(name = "filename")
  private String fileName;

  public String getSerialRecord() {
    return serialRecord;
  }

  public void setSerialRecord(String serialRecord) {
    this.serialRecord = serialRecord;
  }

  public int getFieldNum() {
    return fieldNum;
  }

  public void setFieldNum(int fieldNum) {
    this.fieldNum = fieldNum;
  }

  public int getRecordNum() {
    return recordNum;
  }

  public void setRecordNum(int recordNum) {
    this.recordNum = recordNum;
  }

  public String getFileFlag() {
    return fileFlag;
  }

  public void setFileFlag(String fileFlag) {
    this.fileFlag = fileFlag;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
}
