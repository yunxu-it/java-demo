package cn.winxo.remote.bocom;

import java.util.List;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 签到交易
 */
@Root(name = "ap")
public class StatusRQ {

  @Element(name = "head")
  private RQHead head;
  @ElementList(inline = true, entry = "body")
  private List<StatusRQB> body;

  public RQHead getHead() {
    return head;
  }

  public void setHead(RQHead head) {
    this.head = head;
  }

  public List<StatusRQB> getBody() {
    return body;
  }

  public void setBody(List<StatusRQB> body) {
    this.body = body;
  }
}
