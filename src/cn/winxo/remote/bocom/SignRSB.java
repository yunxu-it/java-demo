package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 000001 交易签到
 */
@Root
public class SignRSB {

  /**
   * 企业代码
   */
  @Element(name = "corp_no")
  private String corpNo;
  /**
   * 企业用户号
   */
  @Element(name = "user_no")
  private String userNo;

  public String getCorpNo() {
    return corpNo;
  }

  public void setCorpNo(String corpNo) {
    this.corpNo = corpNo;
  }

  public String getUserNo() {
    return userNo;
  }

  public void setUserNo(String userNo) {
    this.userNo = userNo;
  }
}
