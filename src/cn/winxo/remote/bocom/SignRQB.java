package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc <pre>
 *   000001 签到交易
 *   用于CS 启动时，对USB key 的客户号和用户名的验证，由CS 发起，无需企业财务 系统干预
 * </pre>
 */
@Root
public class SignRQB {

  /**
   * 交易代码
   */
  @Element(name = "tran_code")
  private String tranCode;

  public String getTranCode() {
    return tranCode;
  }

  public void setTranCode(String tranCode) {
    this.tranCode = tranCode;
  }
}
