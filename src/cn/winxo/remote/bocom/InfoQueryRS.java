package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc
 */
@Root(name = "ap")
public class InfoQueryRS {
  @Element(name = "head") private ResponseHead head;
  @Element(name = "body") private InfoQueryRSB body;

  public ResponseHead getHead() {
    return head;
  }

  public void setHead(ResponseHead head) {
    this.head = head;
  }

  public InfoQueryRSB getBody() {
    return body;
  }

  public void setBody(InfoQueryRSB body) {
    this.body = body;
  }
}
