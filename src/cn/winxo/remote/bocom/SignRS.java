package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 签到应答报文
 */
@Root(name = "ap")
public class SignRS {

  @Element(name = "head")
  private ResponseHead head;
  @Element(name = "body")
  private SignRSB body;

  public ResponseHead getHead() {
    return head;
  }

  public void setHead(ResponseHead head) {
    this.head = head;
  }

  public SignRSB getBody() {
    return body;
  }

  public void setBody(SignRSB body) {
    this.body = body;
  }
}
