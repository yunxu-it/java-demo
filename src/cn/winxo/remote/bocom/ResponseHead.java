package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 应答报文
 */
@Root
public class ResponseHead {

  /**
   * 交易码
   */
  @Element(name = "tr_code")
  private String transCode;
  /**
   * 企业代码，银行提供
   */
  @Element(name = "corp_no")
  private String corpNo;
  /**
   * 发起方序号
   */
  @Element(name = "req_no", required = false)
  private String requestNo;
  /**
   * 交易序号
   */
  @Element(name = "serial_no", required = false)
  private String serialNo;
  /**
   * 应答流水号
   */
  @Element(name = "ans_no", required = false)
  private String ansNo;
  /**
   * 下笔交易序号
   */
  @Element(name = "next_no", required = false)
  private String nextNo;
  /**
   * 交易日期
   */
  @Element(name = "tr_acdt", required = false)
  private String transDate;
  /**
   * 交易时间
   */
  @Element(name = "tr_time", required = false)
  private String transTime;
  /**
   * 返回码
   */
  @Element(name = "ans_code", required = false)
  private String ansCode;
  /**
   * 返回信息
   */
  @Element(name = "ans_info", required = false)
  private String ansInfo;
  /**
   * 返回附加码
   */
  @Element(name = "particular_code", required = false)
  private String particularCode;
  /**
   * 返回附加信息
   */
  @Element(name = "particular_info", required = false)
  private String particularInfo;
  /**
   * 院子交易数
   */
  @Element(name = "atom_tr_count", required = false)
  private String atomTransCount;
  /**
   * 保留字段
   */
  @Element(name = "reserved", required = false)
  private String reserved;


  public String getTransCode() {
    return transCode;
  }

  public void setTransCode(String transCode) {
    this.transCode = transCode;
  }

  public String getCorpNo() {
    return corpNo;
  }

  public void setCorpNo(String corpNo) {
    this.corpNo = corpNo;
  }

  public String getRequestNo() {
    return requestNo;
  }

  public void setRequestNo(String requestNo) {
    this.requestNo = requestNo;
  }

  public String getSerialNo() {
    return serialNo;
  }

  public void setSerialNo(String serialNo) {
    this.serialNo = serialNo;
  }

  public String getAnsNo() {
    return ansNo;
  }

  public void setAnsNo(String ansNo) {
    this.ansNo = ansNo;
  }

  public String getNextNo() {
    return nextNo;
  }

  public void setNextNo(String nextNo) {
    this.nextNo = nextNo;
  }

  public String getTransDate() {
    return transDate;
  }

  public void setTransDate(String transDate) {
    this.transDate = transDate;
  }

  public String getTransTime() {
    return transTime;
  }

  public void setTransTime(String transTime) {
    this.transTime = transTime;
  }

  public String getAnsCode() {
    return ansCode;
  }

  public void setAnsCode(String ansCode) {
    this.ansCode = ansCode;
  }

  public String getAnsInfo() {
    return ansInfo;
  }

  public void setAnsInfo(String ansInfo) {
    this.ansInfo = ansInfo;
  }

  public String getParticularCode() {
    return particularCode;
  }

  public void setParticularCode(String particularCode) {
    this.particularCode = particularCode;
  }

  public String getParticularInfo() {
    return particularInfo;
  }

  public void setParticularInfo(String particularInfo) {
    this.particularInfo = particularInfo;
  }

  public String getAtomTransCount() {
    return atomTransCount;
  }

  public void setAtomTransCount(String atomTransCount) {
    this.atomTransCount = atomTransCount;
  }

  public String getReserved() {
    return reserved;
  }

  public void setReserved(String reserved) {
    this.reserved = reserved;
  }
}
