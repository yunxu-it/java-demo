package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 签到交易
 */
@Root(name = "ap")
public class SignRQ {

  @Element(name = "head")
  private RQHead head;
  @Element(name = "body")
  private SignRQB body;

  public RQHead getHead() {
    return head;
  }

  public void setHead(RQHead head) {
    this.head = head;
  }

  public SignRQB getBody() {
    return body;
  }

  public void setBody(SignRQB body) {
    this.body = body;
  }
}
