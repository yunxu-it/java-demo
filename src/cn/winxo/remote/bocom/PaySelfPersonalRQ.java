package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 对公进行对私付款 330002
 */
@Root(name = "ap")
public class PaySelfPersonalRQ {

  @Element(name = "head")
  private RQHead head;
  @Element(name = "body")
  private PaySelfPersonalRQB body;

  public RQHead getHead() {
    return head;
  }

  public void setHead(RQHead head) {
    this.head = head;
  }

  public PaySelfPersonalRQB getBody() {
    return body;
  }

  public void setBody(PaySelfPersonalRQB body) {
    this.body = body;
  }
}
