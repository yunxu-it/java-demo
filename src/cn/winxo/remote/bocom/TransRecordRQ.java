package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 210201 （单/批)私本请求交易详情
 */
@Root(name = "rcd")
public class TransRecordRQ {

  /**
   * 卡号
   */
  @Element(name = "card_no")
  private String cardNo;
  /**
   * 户名
   */
  @Element(name = "acname")
  private String accountName;
  /**
   * 卡/折类型，0：卡，1：存折
   */
  @Element(name = "card_flag")
  private String type;
  /**
   * 金额
   */
  @Element(name = "amt")
  private String amount;
  /**
   * 业务编号
   */
  @Element(name = "busi_no", required = false)
  private String busiNo;

  public TransRecordRQ() {
  }

  public TransRecordRQ(String cardNo, String accountName, String amount) {
    this.cardNo = cardNo;
    this.accountName = accountName;
    this.amount = amount;
    this.type = "0";
  }

  public TransRecordRQ(String cardNo, String accountName, String type, String amount) {
    this.cardNo = cardNo;
    this.accountName = accountName;
    this.type = type;
    this.amount = amount;
  }

  public String getCardNo() {
    return cardNo;
  }

  public void setCardNo(String cardNo) {
    this.cardNo = cardNo;
  }

  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getBusiNo() {
    return busiNo;
  }

  public void setBusiNo(String busiNo) {
    this.busiNo = busiNo;
  }
}
