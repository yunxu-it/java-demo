package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc
 */
@Root(name = "ap")
public class PaySelfPersonalRS {

  @Element(name = "head")
  private ResponseHead head;
  @Element(name = "body")
  private PaySelfPersonalRSB body;

  public ResponseHead getHead() {
    return head;
  }

  public void setHead(ResponseHead head) {
    this.head = head;
  }


  public PaySelfPersonalRSB getBody() {
    return body;
  }

  public void setBody(PaySelfPersonalRSB body) {
    this.body = body;
  }
}
