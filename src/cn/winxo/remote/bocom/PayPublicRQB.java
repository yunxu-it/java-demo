package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 210201 （单/批)公(本/他)请求 对公对对公。对公对对私跨行
 */
@Root
public class PayPublicRQB {

  public static final String TransCode = "210201";

  /**
   * 付款人账号
   */
  @Element(name = "pay_acno")
  private String payAccount;
  /**
   * 付款人户名
   */
  @Element(name = "pay_acname")
  private String payAccountName;
  /**
   * 收款方行名
   */
  @Element(name = "rcv_bank_name")
  private String receiveBankName;
  /**
   * 收款方账号
   */
  @Element(name = "rcv_acno")
  private String receiveAccountNo;
  /**
   * 收款方户名
   */
  @Element(name = "rcv_acname")
  private String receiveAccountName;
  /**
   * 收款方交换号
   */
  @Element(name = "rcv_exg_code", required = false)
  private String receiveExchangeCode;
  /**
   * 收款方联行号
   */
  @Element(name = "rcv_bank_no", required = false)
  private String receiveBankNo;
  /**
   * 币种
   */
  @Element(name = "cur_code")
  private String currencyCode;
  /**
   * 金额
   */
  @Element(name = "amt")
  private String amount;
  /**
   * 企业凭证编号
   */
  @Element(name = "cert_no")
  private String certNo;
  /**
   * 附言
   */
  @Element(name = "summary")
  private String summary;
  /**
   * 银行标志
   */
  @Element(name = "bank_flag")
  private int bankFlag;
  /**
   * 同城异地标志
   */
  @Element(name = "area_flag")
  private int areaFlag;


  public PayPublicRQB() {
  }

  public PayPublicRQB(String payAccount, String payAccountName, String receiveBankName,
      String receiveAccountNo, String receiveAccountName, String currencyCode, String amount,
      String certNo, int bankFlag, int areaFlag) {
    this.payAccount = payAccount;
    this.payAccountName = payAccountName;
    this.receiveBankName = receiveBankName;
    this.receiveAccountNo = receiveAccountNo;
    this.receiveAccountName = receiveAccountName;
    this.currencyCode = currencyCode;
    this.amount = amount;
    this.certNo = certNo;
    this.bankFlag = bankFlag;
    this.areaFlag = areaFlag;
  }

  public String getPayAccount() {
    return payAccount;
  }

  public void setPayAccount(String payAccount) {
    this.payAccount = payAccount;
  }

  public String getPayAccountName() {
    return payAccountName;
  }

  public void setPayAccountName(String payAccountName) {
    this.payAccountName = payAccountName;
  }

  public String getReceiveBankName() {
    return receiveBankName;
  }

  public void setReceiveBankName(String receiveBankName) {
    this.receiveBankName = receiveBankName;
  }

  public String getReceiveAccountNo() {
    return receiveAccountNo;
  }

  public void setReceiveAccountNo(String receiveAccountNo) {
    this.receiveAccountNo = receiveAccountNo;
  }

  public String getReceiveAccountName() {
    return receiveAccountName;
  }

  public void setReceiveAccountName(String receiveAccountName) {
    this.receiveAccountName = receiveAccountName;
  }

  public String getReceiveExchangeCode() {
    return receiveExchangeCode;
  }

  public void setReceiveExchangeCode(String receiveExchangeCode) {
    this.receiveExchangeCode = receiveExchangeCode;
  }

  public String getReceiveBankNo() {
    return receiveBankNo;
  }

  public void setReceiveBankNo(String receiveBankNo) {
    this.receiveBankNo = receiveBankNo;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getCertNo() {
    return certNo;
  }

  public void setCertNo(String certNo) {
    this.certNo = certNo;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public int getBankFlag() {
    return bankFlag;
  }

  public void setBankFlag(int bankFlag) {
    this.bankFlag = bankFlag;
  }

  public int getAreaFlag() {
    return areaFlag;
  }

  public void setAreaFlag(int areaFlag) {
    this.areaFlag = areaFlag;
  }
}
