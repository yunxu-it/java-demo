package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/28
 * @desc 210229 （单/批)私他请求
 */
@Root
public class PteOtherTransRequestBody {
  /**
   * 企业凭证编号
   */
  @Element(name = "dealNo") private String dealNo;
  /**
   * 付款人账号
   */
  @Element(name = "payAccNo") private String payAccNo;
  /**
   * 付款户名
   */
  @Element(name = "payAccName") private String payAccName;
  /**
   * 代发组合
   */
  @Element(name = "CAgrNo") private String CAgrNo;
  /**
   * 总笔数
   */
  @Element(name = "totalNum") private String totalNum;
  /**
   * 总金额
   */
  @Element(name = "totalAmt") private String totalAmt;
  /**
   * 代发月份
   */
  @Element(name = "payMonth") private String payMonth;
  /**
   * 回单打印标识
   */
  @Element(name = "unitFlag") private String unitFlag;
  /**
   * 摘要
   */
  @Element(name = "summary") private String summary;
  /**
   * 收款账号
   */
  @Element(name = "recAccNo") private String recAccNo;
  /**
   * 收款账号户名
   */
  @Element(name = "recAccName") private String recAccName;
  /**
   * 付款金额
   */
  @Element(name = "tranAmt") private String tranAmt;
  /**
   * 备注
   */
  @Element(name = "rem") private String rem;

  public PteOtherTransRequestBody(String dealNo, String payAccNo, String payAccName,
      String CAgrNo, String totalNum, String totalAmt, String payMonth, String unitFlag,
      String summary, String recAccNo, String recAccName, String tranAmt, String rem) {
    this.dealNo = dealNo;
    this.payAccNo = payAccNo;
    this.payAccName = payAccName;
    this.CAgrNo = CAgrNo;
    this.totalNum = totalNum;
    this.totalAmt = totalAmt;
    this.payMonth = payMonth;
    this.unitFlag = unitFlag;
    this.summary = summary;
    this.recAccNo = recAccNo;
    this.recAccName = recAccName;
    this.tranAmt = tranAmt;
    this.rem = rem;
  }
}
