package cn.winxo.remote.bocom;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * @author winxo
 * @date 2017/11/24
 * @desc 明细查询应答报文
 */
@Root(name = "ap")
public class HistoryQueryRS {

  @Element(name = "head")
  private ResponseHead head;
  @Element(name = "body")
  private HistoryQueryRSB body;

  public ResponseHead getHead() {
    return head;
  }

  public void setHead(ResponseHead head) {
    this.head = head;
  }

  public HistoryQueryRSB getBody() {
    return body;
  }

  public void setBody(HistoryQueryRSB body) {
    this.body = body;
  }
}
