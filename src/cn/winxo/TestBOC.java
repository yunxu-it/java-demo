package cn.winxo;

import cn.winxo.remote.boc.B2E0002RQ;
import cn.winxo.remote.boc.BocRequest;
import cn.winxo.remote.boc.RequestHead;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.simpleframework.xml.core.Persister;

/**
 * Author: lixiaolong
 * Date: 2017/11/16
 * Email: yunxu.it@outlook.com
 * Desc:
 */
public class TestBOC {
  public static void main(String[] args) {
    generatePteRes();
  }

  private static void generatePteRes() {
    BocRequest bocRequest = new BocRequest();
    bocRequest.setHead(new RequestHead("1", "2", "3", "4", "5", "6"));
    //B2E0001RQ trans = new B2E0001RQ();
    //trans.setCustomDate(new Date().getTime()+"");
    //trans.setCeitInfo("shajkjakj");
    //trans.setLoginPassword("123456");
    List<B2E0002RQ> list = new ArrayList<>();
    B2E0002RQ trans = new B2E0002RQ();
    trans.setCustomDate(new Date().getTime() + "");
    list.add(trans);
    trans = new B2E0002RQ();
    trans.setCustomDate(new Date().getTime() + "");
    list.add(trans);
    bocRequest.setTransList(list);
    bocRequest.setTrans(trans);
    Persister persister = new Persister();
    try {
      persister.write(bocRequest, new File("boc_request.xml"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
