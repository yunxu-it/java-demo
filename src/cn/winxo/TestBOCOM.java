//package cn.winxo;
//
//import cn.winxo.remote.bocom.RQInfoQuery;
//import cn.winxo.remote.bocom2.InfoQueryRQB;
//import java.io.File;
//import java.math.BigDecimal;
//import java.util.ArrayList;
//import java.util.List;
//import org.simpleframework.xml.core.Persister;
//import cn.winxo.remote.bocom.InfoRequestBody;
//import cn.winxo.remote.bocom.BaseData;
//import cn.winxo.remote.bocom.InfoResponseBody;
//import cn.winxo.remote.bocom.PteSelfTransRequestBody;
//import cn.winxo.remote.bocom.PteSelfTransResponseBody;
//import cn.winxo.remote.bocom.ReqTransRecord;
//import cn.winxo.remote.bocom.RequestHead;
//import cn.winxo.remote.bocom.ResTransRecord;
//import cn.winxo.remote.bocom.ResponseHead;
//
///**
// * Author: lixiaolong
// * Date: 2017/11/16
// * Email: yunxu.it@outlook.com
// * Desc:
// */
//public class TestBOCOM {
//  public static void main(String[] args) {
//    //generatePteReq();
//    //generatePteRes();
//    generateRequest();
//    //generateResponse();
//  }
//
//  private static void generatePteRes() {
//    BaseData<ResponseHead, PteSelfTransResponseBody> data =
//        new BaseData<>();
//    ResponseHead head =
//        new ResponseHead("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1");
//    data.setHead(head);
//    List<ResTransRecord> resTransRecords = new ArrayList<>();
//    for (int i = 0; i < 3; i++) {
//      String s = i + "";
//      ResTransRecord resTransRecord = new ResTransRecord(s, s, s);
//      resTransRecords.add(resTransRecord);
//    }
//    PteSelfTransResponseBody body =
//        new PteSelfTransResponseBody("1", "1", "1", "1", resTransRecords);
//    data.setBody(body);
//    Persister persister = new Persister();
//    try {
//      persister.write(data, new File("pteResponse.xml"));
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//
//  private static void generatePteReq() {
//    BaseData<RequestHead, PteSelfTransRequestBody> data =
//        new BaseData<>();
//    RequestHead requestHead = new RequestHead("1", "2", "2", "2", "2", "2", "2", "2", "2");
//    data.setHead(requestHead);
//    List<ReqTransRecord> reqTransRecords = new ArrayList<>();
//    for (int i = 0; i < 3; i++) {
//      String s = i + "";
//      ReqTransRecord reqTransRecord = new ReqTransRecord(s, s, s, s, s);
//      reqTransRecords.add(reqTransRecord);
//    }
//    PteSelfTransRequestBody body =
//        new PteSelfTransRequestBody("1", "1", "1", "1", "1", "1", "1", "1", "1", reqTransRecords);
//    data.setBody(body);
//    Persister persister = new Persister();
//    try {
//      persister.write(data, new File("pteRequest.xml"));
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//
//  private static void generateResponse() {
//    BaseData<ResponseHead, InfoResponseBody> data = new BaseData<>();
//    ResponseHead head =
//        new ResponseHead("1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1", "1");
//    data.setHead(head);
//    InfoResponseBody responseBody = new InfoResponseBody("3", "3", "3", "3", "3");
//    data.setBody(responseBody);
//    Persister persister = new Persister();
//    try {
//      persister.write(data, new File("response.xml"));
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//
//  private static void generateRequest() {
//    RQInfoQuery query = new RQInfoQuery();
//    RequestHead requestHead = new RequestHead("1", "2", "2", "2", "2", "2", "2", "2", "2");
//    query.setHead(requestHead);
//    List<InfoQueryRQB> infoQuerys = new ArrayList<>();
//    for (int i = 0; i < 3; i++) {
//      InfoQueryRQB requestBody = new InfoQueryRQB();
//      requestBody.setAccountNo("44445522");
//      infoQuerys.add(requestBody);
//    }
//    query.setBody(infoQuerys);
//    Persister persister = new Persister();
//    try {
//      persister.write(query, new File("request.xml"));
//    } catch (Exception e) {
//      e.printStackTrace();
//    }
//  }
//}
